// ==UserScript==
// @name 			Replace String (PornoLab Ads)
// @author 			pusspounder
// @description     Replace advert strings with blank strings.
// @downloadURL     https://gitlab.com/pusspounder/Userscripts/raw/master/replace-string-pornolab.user.js
// @grant
// @homepageURL		https://gitlab.com/pusspounder/
// @icon
// @include         http*://pornolab.*
// @namespace       pusspounder
// @require
// @run-at
// @version         1.37
// ==/UserScript==

// Source:
// https://greasyfork.org/en/forum/discussion/5112/replace-text-on-website-only-if-the-userscript-sees-trigger-word-greasemonkey

(function () { 'use strict';

/*
NOTE:
You can use \\* to match actual asterisks instead of using it as a wildcard!
The examples below show a wildcard in use and a regular asterisk replacement.
*/

var words = {

//////////////////////////////////////////////////////////////////////////////

// Syntax: 'Search word' : 'Replace word',

'Pornolab рекомендует:' : '',
'VsexShop.ru' : '',
'Аса Акиры!' : '',
'в рот' : '',
'вагина Беладонны!' : '',
'вагина' : '',
'Вагины звезд' : '',
'Вагины' : '',
'Вероники Риччи!' : '',
'Джази Берлин!' : '',
'Женские анусы' : '',
'Женские попки!' : '',
'звезд' : '',
'Копии членов звезд!' : '',
'купить анусы' : '',
'купить попку' : '',
'мастурбаторы' : '',
'мастурбация в рот' : '',
'мастурбация' : '',
'окончание в рот' : '',
'окончание' : '',
'попка Тори Блэк' : '',
'Попки звезд' : '',
'Попки' : '',
'ротики мастурбаторы' : '',
'ротики' : '',
'Скачай попку-мастурбатор' : '',
'Скачай член' : '',
'Скачай' : '',
'Только ХИТЫ' : '',
'Тори Блэк' : '',
'Фаллосы звезд!' : '',
'член' : '',
'Члены звезд!' : '',
'Члены порно актеров!' : '',
'Члены' : '',
'Чпокай звезду в попку!' : '',

// number of lines: 37

//////////////////////////////////////////////////////////////////////////////

'':''};

//////////////////////////////////////////////////////////////////////////////
// This is where the real code is
// Don't edit below this
//////////////////////////////////////////////////////////////////////////////

var regexs = [], replacements = [],
tagsWhitelist = ['PRE', 'BLOCKQUOTE', 'CODE', 'INPUT', 'BUTTON', 'TEXTAREA'],
rIsRegexp = /^\/(.+)\/([gim]+)?$/,
word, text, texts, i, userRegexp;

// prepareRegex by JoeSimmons
// used to take a string and ready it for use in new RegExp()
function prepareRegex(string) {
return string.replace(/([\[\]\^\&\$\.\(\)\?\/\\\+\{\}\|])/g, '\\$1');
}

// function to decide whether a parent tag will have its text replaced or not
function isTagOk(tag) {
return tagsWhitelist.indexOf(tag) === -1;
}

delete words['']; // so the user can add each entry ending with a comma,
// I put an extra empty key/value pair in the object.
// so we need to remove it before continuing

// convert the 'words' JSON object to an Array
for (word in words) {
if ( typeof word === 'string' && words.hasOwnProperty(word) ) {
userRegexp = word.match(rIsRegexp);

// add the search/needle/query
if (userRegexp) {
regexs.push(
new RegExp(userRegexp[1], 'g')
);
} else {
regexs.push(
new RegExp(prepareRegex(word).replace(/\\?\*/g, function (fullMatch) {
return fullMatch === '\\*' ? '*' : '[^ ]*';
}), 'g')
);
}

// add the replacement
replacements.push( words[word] );
}
}

// do the replacement
texts = document.evaluate('//body//text()[ normalize-space(.) != "" ]', document, null, 6, null);
for (i = 0; text = texts.snapshotItem(i); i += 1) {
if ( isTagOk(text.parentNode.tagName) ) {
regexs.forEach(function (value, index) {
text.data = text.data.replace( value, replacements[index] );
});
}
}
}());